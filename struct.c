#include<stdio.h>
#include<string.h>

struct Student{
    char fname[20];
    char subject[20];
    int marks;
};

int main(){

    struct Student std[5];

    for(int i =0; i<5;i++){

        printf("Enter marks for student %d: \n",i+1);

        printf("First name : ");
        scanf("%s",&std[i].fname);

        printf("Subject : ");
        scanf("%s",&std[i].subject);

        printf("Marks : ");
        scanf("%d",&std[i].marks);
    }

    printf("\n ----------------------------\n");

    for(int i = 0; i<5;i++){

        printf("Student %d \n",i+1);
        printf("Name : %s \n",std[i].fname);
        printf("Subject : %s \n",std[i].subject);
        printf("Marks : %d \n",std[i].marks);
    }

return 0;

}